import expect from 'expect' //used any testing library
import metadataAPI from '../../js/api/MetadataAPI';
import Promise from 'bluebird';


describe('async actions', () => {
  var server;

  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/questionMetaData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);
  });

  it('fetch Question Metadata should work fine when mocking service', () => {
    let servicecall = metadataAPI.get_QMD_Data();
    server.respond();
    servicecall.then(function(data){ 
        console.log("data", data);
        expect(data.text).toEqual(JSON.stringify([{"uuid":"d123654"}]));
      }, function(error){
        console.log("error 3456 -->",error);
      });
  });

  after(function () { 
    server.restore(); 
  });
});


describe('async actions', () => {
  var server;

  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/questionMetaData", [
    404, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"Candidate2"}])
    ]);
  });

  it('fetch Question Metadata should work fine when mocking service with Error ', () => {
    let servicecall = metadataAPI.get_QMD_Data();
    server.respond();
    servicecall.then(function(data){ 
        console.log("data", data);
        expect(data.text).toEqual(JSON.stringify([{"uuid":"Candidate2"}]));
      }, function(error){
        console.log("error 11111 -->",error);
      });
  });

  after(function () { 
    server.restore(); 
  });
});


describe('async actions', () => {
  var server;

  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/saveQuestionMetaData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d1234"}])
    ]);
  });
 let setValues = [{
        uuid : 'saw12w32 ' , 
        assTitle:'wear',
      quesName:'qwes',
      uri:' ',
      enabObj:'' ,
      contentTypeData: [],
      audienceRolesData: [],
      difficultyLevelData: [],
      knowledgeLevelData: [],
      alignmentTypeData: [],
      timeReq:'' ,
      desc:'',
      tags: []
      }];
  it('Save Question Metadata should work fine when mocking service', () => {
    let servicecall = metadataAPI.save_QMD_Data(setValues);
    server.respond();
    servicecall.then(function(data){ 
        console.log("data", data);
         expect(data.text).toEqual(JSON.stringify([{"uuid":"d1234"}]));
      }, function(error){
        console.log("error 33333 -->",error);
      });
  });

  after(function () { 
    server.restore(); 
  });
});

describe('async actions', () => {
  var server;

  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/saveQuestionMetaData", [
    404, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d5554"}])
    ]);
  });

  it('Save Question Metadata should work fine when mocking service with Error ', () => {
    let servicecall = metadataAPI.save_QMD_Data();
    server.respond();
    servicecall.then(function(data){ 
        console.log("data", data);
        expect(data.text).toEqual(JSON.stringify([{"uuid":"d5554"}]));
      }, function(error){
        console.log("error 777 -->",error);
      });
  });

  after(function () { 
    server.restore(); 
  });




/*
  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/saveQuestionMetaData", [
    404, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d5554"}])
    ]);
  });

  it('Save Question Metadata should work fine when mocking service with Error ', () => {
    let servicecall = metadataAPI.save_QMD_Data();
    server.respond();
    servicecall.then(function(data){ 
        //console.log("data", data);
        //expect(data.text).toEqual(JSON.stringify([{"uuid":"d5554"}]));
      }, function(error){
        console.log("erroeeeeeeeee -->",error);
        expect(error.message).toEqual("cannot GET http://localhost:3000/saveQuestionMetaData (404)");
      });
  });

  after(function () { 
    server.restore(); 
  });
  */
});




















