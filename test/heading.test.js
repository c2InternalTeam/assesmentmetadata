let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let Heading = require('../js/components/common/Heading'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('Heading testcases', function () {
  before('before creating Heading', function() {
    this.HeadingComp = TestUtils.renderIntoDocument(<Heading />);
    this.heading = TestUtils.findRenderedDOMComponentWithTag(this.HeadingComp, 'h3');
  });

  it('Renders heading component with h3 tag', function() {
  this.HeadingComp = TestUtils.renderIntoDocument(<Heading />); 
   let renderedDOM = ReactDOM.findDOMNode(this.HeadingComp);
    expect(renderedDOM.tagName).to.equal("h3");
  });

  it('passing "text" property updates state and DOM element', function () {
    let HeadingComp = TestUtils.renderIntoDocument(<Heading text='sample text'/>);
    let heading = TestUtils.findRenderedDOMComponentWithTag(HeadingComp, 'h3');
     expect(ReactDOM.findDOMNode(heading).getAttribute("text")).to.equal('sample text');
  });
});