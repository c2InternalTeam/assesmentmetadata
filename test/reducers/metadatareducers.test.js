import MVMConstants from '../../js/constants/MVMConstants';
import metadatareducers from '../../js/reducers/metadatareducers'
import * as actions from '../../js/action/metadataAction'
import expect from 'expect'

describe('Question Level Metadata reducers', () => {
   
   
   let initilizeValues = [{
        uuid : ' ' , 
        assTitle:' ',
  		quesName:' ',
  		uri:' ',
  		enabObj:'' ,
  		contentTypeData: [],
  		audienceRolesData: [],
  		difficultyLevelData: [],
  		knowledgeLevelData: [],
  		alignmentTypeData: [],
  		timeReq:'' ,
  		desc:'',
  		tags: []
      }]

    before('before creating ', function() {
   
   this.QMD_Data= {
  "uuid":"111ui1",
  "assTitle":"Chapter Quiz:The Course of Empire:Expansion and",
  "quesName":"Question1",
  "uri" : " ",
  "contentType":"Journal",
  "audRoles":"Professor",
  "diffLevel":"Medium",
  "alignType":"Align Type 2",
  "knowLevel":"Practioner",
  "timeReq":"",
  "desc":"Sample Question",
  "enabObj":"To add metadata to a question",
  "audienceRolesData":[{"id":0,"label":"Select Audience Role"},
                    {"id":5,"label":"Student"},
                    {"id":6,"label":"Journalist"},
                    {"id":7,"label":"Professor"},
                    {"id":8,"label":"Professionals"}
    ],
    "contentTypeData":[{"id":0,"label":"Select Content Type"},
        {"id":9,"label":"Journal"},
        {"id":10,"label":"Fiction"},
        {"id":11,"label":"Law"},
        {"id":12,"label":"Story Books"}
    ],
    "difficultyLevelData":[{"id":0,"label":"Select Difficulty Level"},
        {"id":13,"label":"Easy"},
        {"id":14,"label":"Medium"},
        {"id":15,"label":"High"}
    ],
    "knowledgeLevelData":[{"id":0,"label":"Select Knowledge Level"},
        {"id":17,"label":"Expert"},
        {"id":19,"label":"Practioner"},
        {"id":20,"label":"Beginner"}
    ],
    "alignmentTypeData":[{"id":0,"label":"Select Alignment Type"},
        {"id":21,"label":"Align Type 1"},
        {"id":22,"label":"Align Type 2"},
        {"id":23,"label":"Align Type 3"},
        {"id":24,"label":"Align Type 4"}
    ],
    "tags": [{
        "id": "1",
        "name": "Whitman"
    }, {
      "id": "2",
      "name": "Dickinson"
    }],
    "suggestions": [{
      "id": "3",
      "name": "English"
    }, {
      "id": "4",
      "name": "Poetry"
    }, {
      "id": "5",
      "name": "mathematics"
  }]
};

 });



     
it('should handle initial state', () => {
     
      let ret = metadatareducers(undefined,{});

      expect(ret).toEqual([{
      	 uuid : ' ' , 
         assTitle:' ',
         quesName:' ',
         uri:' ',
         enabObj:'' ,
         contentTypeData: [],
         audienceRolesData: [],
         difficultyLevelData: [],
  		 knowledgeLevelData: [],
  		 alignmentTypeData: [],
  		 timeReq:'' ,
  		 desc:'',
  		 tags: []

      }]);

     });

it('should handle SAVE_METADATA', () => {
let values={
  "status": "success",
  "tags": [{
    "id": 1,
    "name": "Whitman"
  }, {
    " id": 2,
    "name": "Dickinson"
  }],
  "suggestions": [{
    "id": 3,
    "name": "English"
  }, {
    "id": 4,
    "name": "Poetry"
  }, {
    "id": 5,
    "name": "mathematics"
  }]

};

let nextState = metadatareducers({}, {

        type:MVMConstants.SAVE_METADATA, 
        values:{
  "status": "success",
  "tags": [{
    "id": 1,
    "name": "Whitman"
  }, {
    " id": 2,
    "name": "Dickinson"
  }],
  "suggestions": [{
    "id": 3,
    "name": "English"
  }, {
    "id": 4,
    "name": "Poetry"
  }, {
    "id": 5,
    "name": "mathematics"
  }]

}
          
});

 /*expect(nextState[0]).toEqual({
  "status": "success",
  "tags": [{
    "id": 1,
    "name": "Whitman"
  }, {
    " id": 2,
    "name": "Dickinson"
  }],
  "suggestions": [{
    "id": 3,
    "name": "English"
  }, {
    "id": 4,
    "name": "Poetry"
  }, {
    "id": 5,
    "name": "mathematics"
  }]

});*/

expect(nextState[0].tags).toEqual([{
    "id": 1,
    "name": "Whitman"
  }, {
    " id": 2,
    "name": "Dickinson"
  }]);

expect(nextState[0].suggestions).toEqual([{
    "id": 3,
    "name": "English"
  }, {
    "id": 4,
    "name": "Poetry"
  }, {
    "id": 5,
    "name": "mathematics"
  }]);

});

it('should handle METADATA_GET', () => {

	let QMD_Data={
  "uuid":"111ui1",
  "assTitle":"Chapter Quiz:The Course of Empire:Expansion and",
  "quesName":"Question1",
  "uri" : " ",
  "contentType":"Journal",
  "audRoles":"Professor",
  "diffLevel":"Medium",
  "alignType":"Align Type 2",
  "knowLevel":"Practioner",
  "timeReq":"",
  "desc":"Sample Question",
  "enabObj":"To add metadata to a question",
  "audienceRolesData":[{"id":0,"label":"Select Audience Role"},
                    {"id":5,"label":"Student"},
                    {"id":6,"label":"Journalist"},
                    {"id":7,"label":"Professor"},
                    {"id":8,"label":"Professionals"}
    ],
    "contentTypeData":[{"id":0,"label":"Select Content Type"},
        {"id":9,"label":"Journal"},
        {"id":10,"label":"Fiction"},
        {"id":11,"label":"Law"},
        {"id":12,"label":"Story Books"}
    ],
    "difficultyLevelData":[{"id":0,"label":"Select Difficulty Level"},
        {"id":13,"label":"Easy"},
        {"id":14,"label":"Medium"},
        {"id":15,"label":"High"}
    ],
    "knowledgeLevelData":[{"id":0,"label":"Select Knowledge Level"},
        {"id":17,"label":"Expert"},
        {"id":19,"label":"Practioner"},
        {"id":20,"label":"Beginner"}
    ],
    "alignmentTypeData":[{"id":0,"label":"Select Alignment Type"},
        {"id":21,"label":"Align Type 1"},
        {"id":22,"label":"Align Type 2"},
        {"id":23,"label":"Align Type 3"},
        {"id":24,"label":"Align Type 4"}
    ],
    "tags": [{
        "id": "1",
        "name": "Whitman"
    }, {
      "id": "2",
      "name": "Dickinson"
    }],
    "suggestions": [{
      "id": "3",
      "name": "English"
    }, {
      "id": "4",
      "name": "Poetry"
    }, {
      "id": "5",
      "name": "mathematics"
  }]
};

 let nextState = metadatareducers({}, {

        type:MVMConstants.METADATA_GET, 
        QMD_Data:{
  "uuid":"111ui1",
  "assTitle":"Chapter Quiz:The Course of Empire:Expansion and",
  "quesName":"Question1",
  "uri" : " ",
  "contentType":"Journal",
  "audRoles":"Professor",
  "diffLevel":"Medium",
  "alignType":"Align Type 2",
  "knowLevel":"Practioner",
  "timeReq":"",
  "desc":"Sample Question",
  "enabObj":"To add metadata to a question",
  "audienceRolesData":[{"id":0,"label":"Select Audience Role"},
                    {"id":5,"label":"Student"},
                    {"id":6,"label":"Journalist"},
                    {"id":7,"label":"Professor"},
                    {"id":8,"label":"Professionals"}
    ],
    "contentTypeData":[{"id":0,"label":"Select Content Type"},
        {"id":9,"label":"Journal"},
        {"id":10,"label":"Fiction"},
        {"id":11,"label":"Law"},
        {"id":12,"label":"Story Books"}
    ],
    "difficultyLevelData":[{"id":0,"label":"Select Difficulty Level"},
        {"id":13,"label":"Easy"},
        {"id":14,"label":"Medium"},
        {"id":15,"label":"High"}
    ],
    "knowledgeLevelData":[{"id":0,"label":"Select Knowledge Level"},
        {"id":17,"label":"Expert"},
        {"id":19,"label":"Practioner"},
        {"id":20,"label":"Beginner"}
    ],
    "alignmentTypeData":[{"id":0,"label":"Select Alignment Type"},
        {"id":21,"label":"Align Type 1"},
        {"id":22,"label":"Align Type 2"},
        {"id":23,"label":"Align Type 3"},
        {"id":24,"label":"Align Type 4"}
    ],
    "tags": [{
        "id": "1",
        "name": "Whitman"
    }, {
      "id": "2",
      "name": "Dickinson"
    }],
    "suggestions": [{
      "id": "3",
      "name": "English"
    }, {
      "id": "4",
      "name": "Poetry"
    }, {
      "id": "5",
      "name": "mathematics"
  }]
}
          
       });

  expect(nextState[0]).toEqual({
  "uuid":"111ui1",
  "assTitle":"Chapter Quiz:The Course of Empire:Expansion and",
  "quesName":"Question1",
  "uri" : " ",
  "contentType":"Journal",
  "audRoles":"Professor",
  "diffLevel":"Medium",
  "alignType":"Align Type 2",
  "knowLevel":"Practioner",
  "timeReq":"",
  "desc":"Sample Question",
  "enabObj":"To add metadata to a question",
  "audienceRolesData":[{"id":0,"label":"Select Audience Role"},
                    {"id":5,"label":"Student"},
                    {"id":6,"label":"Journalist"},
                    {"id":7,"label":"Professor"},
                    {"id":8,"label":"Professionals"}
    ],
    "contentTypeData":[{"id":0,"label":"Select Content Type"},
        {"id":9,"label":"Journal"},
        {"id":10,"label":"Fiction"},
        {"id":11,"label":"Law"},
        {"id":12,"label":"Story Books"}
    ],
    "difficultyLevelData":[{"id":0,"label":"Select Difficulty Level"},
        {"id":13,"label":"Easy"},
        {"id":14,"label":"Medium"},
        {"id":15,"label":"High"}
    ],
    "knowledgeLevelData":[{"id":0,"label":"Select Knowledge Level"},
        {"id":17,"label":"Expert"},
        {"id":19,"label":"Practioner"},
        {"id":20,"label":"Beginner"}
    ],
    "alignmentTypeData":[{"id":0,"label":"Select Alignment Type"},
        {"id":21,"label":"Align Type 1"},
        {"id":22,"label":"Align Type 2"},
        {"id":23,"label":"Align Type 3"},
        {"id":24,"label":"Align Type 4"}
    ],
    "tags": [{
        "id": "1",
        "name": "Whitman"
    }, {
      "id": "2",
      "name": "Dickinson"
    }],
    "suggestions": [{
      "id": "3",
      "name": "English"
    }, {
      "id": "4",
      "name": "Poetry"
    }, {
      "id": "5",
      "name": "mathematics"
  }]
});


        
});

});

 