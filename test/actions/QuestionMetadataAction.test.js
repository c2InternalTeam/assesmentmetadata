import * as actions from '../../js/action/MetadataAction'
import expect from 'expect' //used any testing library
import metadataConstants from '../../js/constants/MVMConstants';
import marklogicMetadataAPI from '../../js/api/MetadataAPI';
import store from '../../js/store';
import Promise from 'bluebird';

describe('async actions', () => {
  var dispacthSpy;
  var apiSpy;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     apiSpy = sinon.spy(marklogicMetadataAPI,"get_QMD_Data");
  });

 it('should return a function', () => {
    expect(actions.fetchMetaData()).toBeA('function');
  })

 it('api should be called when fetching QMD data', () => {
    actions.fetchMetaData()(store.dispatch);
    expect(apiSpy.called).toEqual(true);
 })

  after(function () { 
    dispacthSpy.restore();
    apiSpy.restore();
  });
});


describe('stub api and returning sucess should work', () => {
  var dispacthSpy;
  let data;
  before(function () {
    data = {"qmddata":"abcdefg"};
    data = JSON.stringify(data);
     sinon.stub(marklogicMetadataAPI, "get_QMD_Data").returns(Promise.resolve({"text":data}));
     dispacthSpy = sinon.spy(store,"dispatch");
  });
  it('dispatch a QuestionMetaData action', () => {
    actions.fetchMetaData()(store.dispatch);
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(metadataConstants.METADATA_GET);
      debugger;
      expect(dispatchArg.QMD_Data.qmddata).toEqual("abcdefg");
      
    },1000)
  });
  after(function () { 
     dispacthSpy.restore();
     marklogicMetadataAPI.get_QMD_Data.restore();
  });
});


describe('stub api and returning error should work', () => {
  var dispacthSpy1;
  let data;
  before(function () {
    data = {"qmddata":"abcdefg"};
    data = JSON.stringify(data);

     sinon.stub(marklogicMetadataAPI, "get_QMD_Data").returns(Promise.reject({"error":"error"}));
     dispacthSpy1 = sinon.spy(store,"dispatch");
  });
  it('dispatch a QuestionMetaData action', () => {
    actions.fetchMetaData()(store.dispatch);
    setTimeout(function(){
      expect(dispacthSpy1.called).toEqual(false); 
    },1000)
  });
  after(function () { 
    dispacthSpy1.restore();
     marklogicMetadataAPI.get_QMD_Data.restore();
  });
});

describe('async actions', () => {
  var dispacthSpy;
  var server;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/questionMetaData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify({"uuid":"d123654"})
    ]);

  });


  it('fetch Question Metadata should work fine when mocking service', () => {
    actions.fetchMetaData()(store.dispatch);
    server.respond();
    console.log("server respond called");
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(metadataConstants.METADATA_GET);
      debugger;
      expect(dispatchArg.QMD_Data.uuid).toEqual("d123654");
      
    },100)
  });

  after(function () { 
    dispacthSpy.restore();
    server.restore(); 
  });
});


/***********For saveMetaData Function ********/
describe('async actions', () => {
  var dispacthSpy;
  var apiSpy;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     apiSpy = sinon.spy(marklogicMetadataAPI,"save_QMD_Data");
  });

 it('should return a function', () => {
    expect(actions.saveMetaData()).toBeA('function');
  })

 it('api should be called when Saving QuestionMetaData ', () => {
    actions.saveMetaData()(store.dispatch);
    expect(apiSpy.called).toEqual(true);
 })

  after(function () { 
    dispacthSpy.restore();
    apiSpy.restore();
  });
});


describe('stub api and returning sucess should work', () => {
  var dispacthSpy;
  let data;
  before(function () {
    data = {"saveqmddata":"abcdefg"};
    data = JSON.stringify(data);
     sinon.stub(marklogicMetadataAPI, "save_QMD_Data").returns(Promise.resolve({"text":data}));
     dispacthSpy = sinon.spy(store,"dispatch");
  });
  it('dispatch a Save QuestionMetaData action', () => {
    actions.saveMetaData()(store.dispatch);
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(metadataConstants.SAVE_METADATA);
      debugger;
      expect(dispatchArg.QMD_Data.saveqmddata).toEqual("abcdefg");
      
    },1000)
  });
  after(function () { 
     dispacthSpy.restore();
     marklogicMetadataAPI.save_QMD_Data.restore();
  });
});


describe('stub api and returning error should work', () => {
  var dispacthSpy1;
  let data;
  before(function () {
    data = {"saveqmddata":"abcdefg"};
    data = JSON.stringify(data);

     sinon.stub(marklogicMetadataAPI, "save_QMD_Data").returns(Promise.reject({"error":"error"}));
     dispacthSpy1 = sinon.spy(store,"dispatch");
  });
  it('dispatch a Save QuestionMetaData action', () => {
    actions.saveMetaData()(store.dispatch);
    setTimeout(function(){
      expect(dispacthSpy1.called).toEqual(false); 
    },1000)
  });
  after(function () { 
    dispacthSpy1.restore();
     marklogicMetadataAPI.save_QMD_Data.restore();
  });
});

describe('async actions', () => {
  var dispacthSpy;
  var server;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/saveQuestionMetaData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify({"uuid":"d123654"})
    ]);

  });


  it('Save Question Metadata should work fine when mocking service', () => {
    actions.saveMetaData()(store.dispatch);
    server.respond();
    console.log("server respond called");
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(metadataConstants.SAVE_METADATA);
      debugger;
      expect(dispatchArg.QMD_Data.uuid).toEqual("d123654");
      
    },100)
  });

  after(function () { 
    dispacthSpy.restore();
    server.restore(); 
  });
});