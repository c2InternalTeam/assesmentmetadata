/*
 *
 *
 * 
 */
import React from 'react';


class SelectBox extends React.Component{
constructor(props) {
    super(props);
    this.displayName = 'SelectBox';
    this.handleChange = this.handleChange.bind(this);
}
static propTypes= {
    id:React.PropTypes.string.isRequired,
    multiple: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    required: React.PropTypes.bool,
    autofocus: React.PropTypes.bool,
    defaultValue:React.PropTypes.string,
        options:React.PropTypes.array.isRequired,
}
static defaultProps ={
      id:'',
      multiple: false,
      disabled:false,
      required:false,
      autofocus: false,
      defaultValue:'',
          options:[]
}
componentWillMount() {
        var defaultValue = this.props.defaultValue ;
        if(this.props.options!==undefined && this.props.options.length>0){
            this.props.options.map(function(CurrOption){ 
               if(defaultValue === CurrOption.text){
                    CurrOption.selected = "selected";
                }else{
                    CurrOption.selected = "";
                }
            });
        }
}
componentDidMount() {
    if (this.props.autofocus){
      //this.refs.select.focus();
    }      
}

componentWillReceiveProps (){
    var defaultValue = this.props.defaultValue ;
        if(this.props.options!==undefined && this.props.options.length>0){
            this.props.options.map(function(CurrOption){ 
               if(defaultValue === CurrOption.text){
                    CurrOption.selected = "selected";
                }else{
                    CurrOption.selected = "";
                }
            });
        }
}

handleChange(e) {
        if (this.props.handleChange) {
            this.props.handleChange({"id":e.target.id,"value":e.target.value.trim()});
        }
}
render() {
        return (
            <select id={this.props.id} multiple={this.props.multiple} disabled={this.props.disabled} required={this.props.required}
      ref="select" onChange ={this.handleChange}>
                {this.props.options.map((CurrOption, index) =>
                <option value = {CurrOption.value}  key={CurrOption.value} selected={CurrOption.selected}>{CurrOption.text}</option>
            )}
            </select>
        )
    }

};

module.exports = SelectBox;
