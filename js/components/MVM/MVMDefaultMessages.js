import {defineMessages} from 'react-intl';
export const messages = defineMessages({
	Question_Keyword_Tags: {
        id: 'Question_Keyword_Tags',
        defaultMessage: 'Question Keyword Tags'
    },
    MVM_Data: {
        id: 'MVM_Data',
        defaultMessage: 'Metadata'
    },
    MVM_UUID: {
    	id: 'MVM_UUID',
    	defaultMessage: 'UUID'
    },
    File_Name: {
    	id:'File_Name',
    	defaultMessage: 'Filename'
    },
    MVM_Name: {
    	id:'MVM_Name',
    	defaultMessage: 'Name (required)'
    },
    MVM_URN: {
    	id:'MVM_URN',
    	defaultMessage: 'URN'
    },
    MVM_Keywords:{
        id:'MVM_Keywords',
        defaultMessage: 'Keywords'
    },
    Content_Type: {
    	id:'Content_Type',
    	defaultMessage: 'Content Type'
    },
    Audience_Role: {
    	id:'Audience_Role',
    	defaultMessage: 'Audience Role'
    },
    Difficult_Level: {
    	id:'Difficult_Level',
    	defaultMessage: 'Difficult Level'
    },
    Knowledge_Level: {
    	id:'Knowledge_Level',
    	defaultMessage: 'Knowledge Level'
    },
    Publisher: {
    	id:'Publisher',
    	defaultMessage: 'Publisher'
    },
    Discipline: {
        id:'Discipline',
        defaultMessage: 'Discipline/Domain'
    },
    Objective_Alignment: {
    	id: 'Objective_Alignment',
    	defaultMessage: 'Objective Alignment'
    },
    Goal_Alignment: {
        id: 'Goal_Alignment',
        defaultMessage: 'Goal Alignment'
    },
    Time_Required: {
    	id: 'Time_Required',
    	defaultMessage: 'Time Requried'
    },
    MVM_Desc: {
    	id: 'MVM_Desc',
    	defaultMessage: 'Description'
    },
    MVM_PLAN_ID: {
        id: 'MVM_PLAN_ID',
        defaultMessage: 'Plan ID'
    },
    MVM_Save_Button:{
    	id:'MVM_Save_Button',
    	defaultMessage: 'Save'
    },
    MVM_ISBN:{
        id:'MVM_ISBN',
        defaultMessage: 'ISBN'
    },
     MVM_Module_No:{
        id:'MVM_Module_No',
        defaultMessage: 'Module No'
    },
    MVM_Chapter_No:{
        id:'MVM_Chapter_No',
        defaultMessage: 'Chapter No'
    },
    MVM_Author:{
        id:'MVM_Author',
        defaultMessage: 'Author'
    },
    MVM_Product:{
        id:'MVM_Product',
        defaultMessage: 'Product'
    }
})