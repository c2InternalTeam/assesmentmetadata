/*
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *.
 *
 * LinkMetaDataActions
 */
import MetaDataApi from '../api/MetadataApi'
import * as types from '../constants/MVMConstants'
import MetaDataService from '../common/util/metadataService';
import MDSConstants from '../constants/MDSConstants'

/*  export function  fetchMetaData (){
    return dispatch => {
      MetaDataApi.get_QMD_Data().then(function(data){ 
        dispatch({
          type: types.METADATA_GET,
          QMD_Data : JSON.parse(data.text)
        })
      })
  }
}

  export function  saveMetaData (values){
    return dispatch => {
      MetaDataApi.save_QMD_Data(values).then(function(data){ 
        dispatch({
          type: types.SAVE_METADATA,
          QMD_Data : JSON.parse(data.text),values
        })
      })
  }
}*/

export function  fetchMetaData (metadata){debugger;
    return (dispatch, getState) => {
      let state = getState();
      let bufferGet = MDSConstants.BUFFER_GET;
      let metaDataState = state.Metadatareducers;
      //make service call get the metadata based on uuid
      //metaDataState.uuid = "a92df0fa-d68d-41fe-9594-a0fdd5cf43e3";
      //metaDataState.uuid = "872c79ad-c284-4bac-bf3a-a12048674f41";
      //metaDataState.uuid = "60cebec2-19cc-4f40-9903-94bc4562f550";
      if(metaDataState && metaDataState.uuid && metaDataState.uuid!==''){
          bufferGet.data.uuid = metaDataState.uuid;
          let promise = MetaDataService.send(bufferGet);
          promise.then(function (replyGet) {
            var itemArr = [];debugger;
            if(typeof replyGet.keywords === 'Array'){
 
                for(var i=0; i<replyGet.keywords.length;i++){
        console.log('name : '+replyGet.keywords[i]);
 
        itemArr.push({"id": i,"name" : replyGet.keywords[i]});
     }
            }else if(typeof replyGet.keywords === 'string'){
              itemArr.push({"id": 0,"name" : replyGet.keywords});
            }
            replyGet.keywords = itemArr;
              for (var key in replyGet) {
                  metadata[key] = replyGet[key];
              }
              console.log('Button:onClick - create send successful response received %o', replyGet);
              dispatch({
                type: types.SET_UUID,
                QMD_Data : metadata
              })
            },function (replyGet) {
              console.log('Button:onClick - create send failed error message received %o', replyGet);
          });
        

      }else{
        dispatch({
          type: types.METADATA_GET,
          QMD_Data : metadata
        })
      } 
    }
}

export function  fetchMetaDataTaxonomy (){
    return dispatch => {
      MetaDataApi.get_QMD_Data().then(function(data){ 
        dispatch(fetchMetaData(JSON.parse(data.text)))
      })
  }
}

export function  saveMetaData (values){debugger;
    return dispatch => {
      let buffer = MDSConstants.BUFFER;
      //buffer.data = values;
        /*for (var name in values.keywords) {
                  console.log('key : '+name);

              }*/
      var itemArr = [];
     for(var i=0; i<values.keywords.length;i++){
        console.log('name : '+values.keywords[i].name);
        itemArr.push(values.keywords[i].name);
     }
     console.log('itemArr : '+itemArr);
     values.keywords = itemArr;
     buffer.data = values;
      let promise = MetaDataService.send(buffer);
        promise.then(function (replyCreate) {
            values.uudi = replyCreate.uuid;
            console.log('Button:onClick - create send successful response received %o', replyCreate);
            dispatch({
              type: types.SET_UUID,
              QMD_Data : values
            })
          },function (replyCreate) {
            console.log('Button:onClick - create send failed error message received %o', replyCreate);
        });
     
  }
}

export function populateAutoComplete(text) {
    return dispatch => {
      MetaDataApi.get_AutoComplete_Data(text).then(function(data){ 
        dispatch({
          type: types.AUTO_COMPLETE,
          data: JSON.parse(data.text),
          text
        });
      });
  }
}