import * as CONST from '../constants/MVMConstants' 

var initilizeData = [{

  }]

const autoComplete = (state = initilizeData, action) => {
  switch (action.type) {
    case CONST.AUTO_COMPLETE:
      return[
        ...state, {
            data:action.data,
            text: action.text
        }
      ]
    default:
      return state
  }
}

export default autoComplete
