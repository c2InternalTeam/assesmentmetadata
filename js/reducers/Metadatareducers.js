import { METADATA_GET, SAVE_METADATA } from '../constants/MVMConstants'
import Immutable from 'immutable'

let initilizeValues = [{
  uuid : ' ' , 
  filename:' ',
  name:' ',
  urn:' ',
  enabObj:'' ,
  planId: '',
  modNo:'',
  chapNo:'',
  author:'',
  contentTypeData: [],
  audienceRolesData: [],
  difficultyLevelData: [],
  knowledgeLevelData: [],
  alignmentTypeData: [],
  disciplineData: [],
  goalAlignmentData: [],
  languages: [],
  timeReq:'' ,
  isbn: '',
  desc:'',
  keywords: [],
  product:''
}]

const Metadatareducers = (state = initilizeValues, action)=>{
  switch(action.type) {
    
    case 'METADATA_GET':
    debugger;
    if(!state && !state[0]){
      return[
         Immutable.fromJS(state[0]).merge(Immutable.Map(action.QMD_Data)).toJS()
      ]
    }else{
      return[
        Immutable.fromJS({
        uuid : ' ' , 
        filename:' ',
        name:' ',
        urn:' ',
        enabObj:'' ,
        planId: '',
        isbn: '',
        modNo:'',
        chapNo:'',
        author:'',
        contentTypeData: [],
        audienceRolesData: [],
        difficultyLevelData: [],
        knowledgeLevelData: [],
        alignmentTypeData: [],
        disciplineData: [],
        goalAlignmentData: [],
        languages: [],
        timeReq:'' ,
        desc:'',
        keywords: [],
         product:''
      }).merge(Immutable.Map(action.QMD_Data)).toJS()
      ]
    }
    break;
    case 'SAVE_METADATA':
    if(!state && !state[0]){
      return[
        Immutable.fromJS(state[0]).merge(Immutable.Map(action.values)).toJS()
      ]
    }else{
      return[
        Immutable.fromJS({
        uuid : ' ' , 
        filename:' ',
        name:' ',
        urn:' ',
        enabObj:'' ,
        planId:'',
        isbn: '',
        modNo:'',
        chapNo:'',
        author:'',
        contentTypeData: [],
        audienceRolesData: [],
        difficultyLevelData: [],
        knowledgeLevelData: [],
        alignmentTypeData: [],
        disciplineData: [],
        goalAlignmentData: [],
        languages: [],
        timeReq:'' ,
        desc:'',
        keywords: [],
         product:''
      }).merge(Immutable.Map(action.values)).toJS()
      ]
    }
    break;
    case 'SET_UUID':
      return[
        Immutable.fromJS(state[0]).merge(action.QMD_Data).toJS()
      ]
    default:
    return state
      
    }
  }

  export default Metadatareducers;