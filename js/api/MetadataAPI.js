import request from 'superagent-bluebird-promise'
import service from '../constants/service'
const TIMEOUT = 100

export default {

    get_QMD_Data(cb, timeout) {
    return request.get(service.questionMetaData).promise();
  },

      save_QMD_Data(value,cb, timeout) {
    return request.get(service.saveQuestionMetaData).promise();
  },

      get_AutoComplete_Data(cb, timeout) {
    return request.get(service.autoCompleteData).promise();
  }

}