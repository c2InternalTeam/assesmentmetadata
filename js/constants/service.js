var baseUrl = "http://localhost:3000/";

var service = {
	questionMetaData: baseUrl + 'questionMetaData',
	saveQuestionMetaData: baseUrl + 'saveQuestionMetaData',
	autoCompleteData: baseUrl + 'autoCompleteData'
}

module.exports = service;