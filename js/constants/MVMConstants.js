var constants = {
	METADATA_GET : 'METADATA_GET',
	SAVE_METADATA : 'SAVE_METADATA',
	AUTO_COMPLETE : 'AUTO_COMPLETE',
	SET_UUID : 'SET_UUID'
}

module.exports = constants;
