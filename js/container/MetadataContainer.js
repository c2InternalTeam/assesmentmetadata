/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
 "use strict";
 import React from 'react';
 import Heading from '../components/common/Heading';
 import MVMComponent from '../components/MVM/MVM';
 import {fetchMetaDataTaxonomy, saveMetaData} from '../action/MetadataAction';
 import ReactTabs from 'react-tabs';
 import { connect } from 'react-redux'


const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }
  return [];
}

const mapStateToProps = (state) => { debugger;
    let metadata = getSelectedValues(state.Metadatareducers);
    var product = '';
         if(state.autoComplete.length > 1){
        product = state.autoComplete[state.autoComplete.length-1].text;
     }
     else{
      product = metadata.product;
     }
     if (metadata.contentTypeData) {
    return {
        suggestions: metadata.suggestions,
        contentTypeData : metadata.contentTypeData,
        audienceRolesData : metadata.audienceRolesData,
        difficultyLevelData : metadata.difficultyLevelData,
        knowledgeLevelData : metadata.knowledgeLevelData,
        alignmentTypeData : metadata.alignmentTypeData,
        disciplineData : metadata.disciplineData,
        goalAlignmentData : metadata.goalAlignmentData,
        languages : metadata.languages,
        uuid : metadata.uuid,
        filename : metadata.filename,
        name : metadata.name,
        urn : metadata.urn,
        planId: metadata.planId,
        isbn: metadata.isbn,
        modNo: metadata.modNo,
        chapNo: metadata.chapNo,
        author: metadata.author,
        contentType : metadata.contentType,
        audience :  metadata.audience,
        difficultyLevel :metadata.difficultyLevel,
        knowledgeLevel : metadata.knowledgeLevel,
        alignType : metadata.alignType,
        discipline : metadata.discipline,
        goalAlignment : metadata.goalAlignment,
        enabObj : metadata.enabObj,
        timeReq : metadata.timeReq,
        desc : metadata.desc,
        keywords: metadata.keywords,
        product: product,
        "initialValues": {
        uuid : metadata.uuid,
        filename : metadata.filename,
        name : metadata.name,
        urn : metadata.urn,
        planId: metadata.planId,
        isbn: metadata.isbn,
        modNo: metadata.modNo,
        chapNo: metadata.chapNo,
        author: metadata.author,
        contentType : metadata.contentType,
        audience :  metadata.audience,
        difficultyLevel :metadata.difficultyLevel,
        knowledgeLevel : metadata.knowledgeLevel,
        alignType : metadata.alignType,
        discipline : metadata.discipline,
        goalAlignment : metadata.goalAlignment,
        enabObj : metadata.enabObj,
        timeReq : metadata.timeReq,
        desc : metadata.desc,
        keywords: metadata.keywords,
        product: metadata.product
      }
    }
  }else{
      return state.form.mvm;
    
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    componentWillMount(){
      //dispatch(fetchMetaData())
      dispatch(fetchMetaDataTaxonomy())
    },
    onSave(values, dispatch){debugger;
      console.log(values);
      var data = values;
      data.contentType = values.contentType.text;
      dispatch(saveMetaData(data));
    }
  }
}

const MetadataContainerConnect = connect(
  mapStateToProps,
  mapDispatchToProps
  )(MVMComponent)

  export default MetadataContainerConnect;
