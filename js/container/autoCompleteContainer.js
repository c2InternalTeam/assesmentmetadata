import { connect } from 'react-redux';
import { populateAutoComplete } from '../action/MetadataAction';
import AutoComplete from '../components/common/autoComplete';

const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }

  return [];
}

const mapStateToProps = (state) => {
  return {
    data: getSelectedValues(state.autoComplete)
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    onSuggestionsUpdateRequested: ({value}) => {
      debugger;
     dispatch(populateAutoComplete(value));
      console.log(value);
    }
  }
}

const AutoCompleteContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AutoComplete)

export default AutoCompleteContainer
